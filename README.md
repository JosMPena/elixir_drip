# ElixirDrip.Umbrella

**What we did here:**
* Created an Umbrella super-app to hold the project structure
* Created an umbrella app which depends on Ecto(DB language). This umbrella app will hold the business logic.
* Created another umbrella all with Phoenix(web framework) which will serve as the web interface for our project.
* Created git repo to control the project on Gitlab

